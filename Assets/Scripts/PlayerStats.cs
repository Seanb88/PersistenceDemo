﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


/// <summary>
/// This is a Serializable class containing all player information
/// </summary>
[Serializable]
public class PlayerStats
{
    public int Health;
    public int Level;
    public PlayerStats()
    {
        Health = 100;
        Level = 1;
    }
}

