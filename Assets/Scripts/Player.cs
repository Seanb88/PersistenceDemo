﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class Player : MonoBehaviour {

    public PlayerStats stats;

    public string SaveFileName = "player.dat";

    private Text HealthText;
    private Text LevelText;
    private string SaveFilePath;

	// Use this for initialization
	void Start ()
    {
        SaveFilePath = Path.Combine(Application.persistentDataPath, SaveFileName);
        stats = LoadPlayerStats();

        HealthText = GameObject.Find("PlayerHealthText").GetComponent<Text>();
        LevelText = GameObject.Find("PlayerLevelText").GetComponent<Text>();

        RefreshUI();

        //Load player data if exists
        LoadPlayerStats();
	}

    void Update()
    {
        //this is all really hacky for example purposes
        if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            stats.Health -= 10;
            RefreshUI();
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            stats.Health += 10;
            RefreshUI();
        }

        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            stats.Level += 1;
            RefreshUI();
        }

        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            stats.Level -= 1;
            RefreshUI();
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            SavePlayerStats();
        }
    }

    void RefreshUI()
    {
        HealthText.text = string.Format("Health: {0}", stats.Health);
        LevelText.text = string.Format("Level: {0}", stats.Level);
    }

    void SavePlayerStats()
    {
        BinaryFormatter bf = new BinaryFormatter();
        using (FileStream fs = File.Open(SaveFilePath, FileMode.OpenOrCreate))
        {
            bf.Serialize(fs, stats);
            print(string.Format("Saved player stats to {0}", SaveFilePath));
        }
    }

    PlayerStats LoadPlayerStats()
    {
        if(File.Exists(SaveFilePath)) //if we have save data
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream fs = File.Open(SaveFilePath, FileMode.Open))
            {
                return (PlayerStats)bf.Deserialize(fs);
            }
        }
        else
        {
            return new PlayerStats(); //first time launching
        }
    }
}
