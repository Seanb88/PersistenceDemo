# PersistenceDemo - Saving and Loading in Unity


Inputs:

Left & Right change the level value.
Up & Down change the health value.
Space saves the game.

### Instructions
Open the main.scene and run the game. Notice that Health and Level defaulted to 100 and 1 respectively.
Press the arrow keys to increment/decrement health and level values. Press space to save the game.


Now re-open the game and you'll see that the values are not the defaults any more. If you're interested in seeing
the save file, the path is printed in the Unity Console when you press space. It is binary encoded though so it won't
make sense to look at.